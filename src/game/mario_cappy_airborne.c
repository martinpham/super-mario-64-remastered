#include "mario_cappy.h"
#include "mario.h"
#include "sm64.h"
#include "mario_step.h"
#include "audio/external.h"

static u8 gWallSlide = TRUE;

void reset_wall_slide() {
    gWallSlide = TRUE;
}

s32 mario_check_wall_slide(struct MarioState *m) {

    // Wall slide available
    if (!gWallSlide) {
        return FALSE;
    }

    // There must be a wall
    if (m->wall == NULL) {
        return FALSE;
    }

    // Mario must not be holding something
    if (m->heldObj != NULL) {
        return FALSE;
    }

    return TRUE;
}

s32 mario_start_wall_slide(struct MarioState *m) {
    
    // Start sliding only when Mario is moving downwards
    // to not end prematurely his jump
    return (m->vel[1] < 0.f);
}

s32 act_wall_slide(struct MarioState *m) {
    mario_set_forward_vel(m, 0);
    set_mario_animation(m, MARIO_ANIM_START_WALLKICK);
    play_sound(SOUND_MOVING_TERRAIN_SLIDE + m->terrainSoundAddend, m->marioObj->header.gfx.cameraToObject);
    m->vel[1] = -6.f;
    m->particleFlags |= PARTICLE_DUST;

    // Wall jump
    if (m->input & INPUT_A_PRESSED) {
        m->vel[1] = 52.0f;
        m->faceAngle[1] += 0x8000;
        mario_set_forward_vel(m, 24.f);
        return set_mario_action(m, ACT_WALL_KICK_AIR, 0);
    }

    // Get out of wall slide
    if (m->input & INPUT_Z_PRESSED) {
        gWallSlide = FALSE;
        play_sound(SOUND_MARIO_UH, m->marioObj->header.gfx.cameraToObject);
        m->input &= (~(INPUT_Z_PRESSED | INPUT_Z_DOWN));
        return set_mario_action(m, ACT_FREEFALL, 0);
    }

    switch (perform_air_step(m, 0)) {
        case AIR_STEP_LANDED:
            return set_mario_action(m, ACT_IDLE, 0);

        case AIR_STEP_HIT_WALL:
            if (!mario_check_wall_slide(m)) {
                return set_mario_action(m, ACT_FREEFALL, 0);
            }
            break;
    }
    return FALSE;
}

s32 act_dive_start(struct MarioState *m) {
    mario_set_forward_vel(m, 40.f);
    m->vel[1] = 40.f;
    m->particleFlags |= PARTICLE_MIST_CIRCLE;
    return set_mario_action(m, ACT_DIVE, 0);
}
