#include "mario_cappy.h"
#include "mario.h"
#include "sm64.h"
#include "mario_step.h"
#include "audio/external.h"

static void mario_set_rolling_vel_and_anim(struct MarioState *m) {
    set_mario_animation(m, MARIO_ANIM_FORWARD_SPINNING);
    mario_set_forward_vel(m, 60.f);
}

s32 act_rolling_start(struct MarioState *m) {
    init_cappy_action_timer(17);
    mario_set_rolling_vel_and_anim(m);
    m->particleFlags |= PARTICLE_HORIZONTAL_STAR;
    return set_mario_action(m, ACT_ROLLING, 0);
}

s32 act_rolling(struct MarioState *m) {

    if (decrease_cappy_action_timer()) {
        return set_mario_action(m, ACT_WALKING, 0);
    }

    if (get_cappy_action_timer() <= 8 && (m->input & INPUT_Z_DOWN) && (m->input & INPUT_B_PRESSED)) {
        return set_mario_action(m, ACT_ROLLING_START, 0);
    }

    if (m->input & INPUT_A_PRESSED) {
        return set_jump_from_landing(m);
    }

    mario_set_rolling_vel_and_anim(m);

    switch (perform_ground_step(m)) {
        case GROUND_STEP_LEFT_GROUND:
            set_mario_animation(m, MARIO_ANIM_GENERAL_FALL);
            return set_mario_action(m, ACT_FREEFALL, 0);

        case GROUND_STEP_HIT_WALL:
            m->particleFlags |= PARTICLE_VERTICAL_STAR;
            return set_mario_action(m, ACT_BACKWARD_GROUND_KB, 0);

        default:
            if ((get_cappy_action_timer() % 8) == 0) {
                play_sound(SOUND_ACTION_TWIRL, m->marioObj->header.gfx.cameraToObject);
            }
            break;
    }
    return FALSE;
}
