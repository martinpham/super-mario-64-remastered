#include "mario_cappy.h"
#include "mario.h"
#include "sm64.h"
#include "engine/math_util.h"
#include "audio/external.h"
#include "mario_actions_submerged.h"
#include "print.h"
#include "gfx_dimensions.h"
#include <stdio.h>

static void mario_set_water_dash_vel_and_anim(struct MarioState *m) {
    set_mario_anim_with_accel(m, MARIO_ANIM_FLUTTERKICK, 0x30000);
    m->particleFlags |= (PARTICLE_PLUNGE_BUBBLE | PARTICLE_BUBBLE);
    m->vel[0] = coss(m->faceAngle[0]) * sins(m->faceAngle[1]) * 56.0f;
    m->vel[1] = sins(m->faceAngle[0]) * 56.0f;
    m->vel[2] = coss(m->faceAngle[0]) * coss(m->faceAngle[1]) * 56.0f;
}

s32 act_water_dash_start(struct MarioState *m) {
    init_cappy_action_timer(21);
    mario_set_water_dash_vel_and_anim(m);
    decrease_O2_level(m, 120);
    return set_mario_action(m, ACT_WATER_DASH, 0);
}

s32 act_water_dash(struct MarioState *m) {

    if (decrease_cappy_action_timer()) {
        return set_mario_action(m, ACT_BREASTSTROKE, 0);
    }

    if (get_cappy_action_timer() <= 6 && (m->input & INPUT_Z_DOWN) && (m->input & INPUT_B_PRESSED)) {
        return set_mario_action(m, ACT_WATER_DASH_START, 0);
    }

    mario_set_water_dash_vel_and_anim(m);

    switch (perform_water_step(m)) {
        case WATER_STEP_HIT_WALL:
            m->particleFlags |= PARTICLE_VERTICAL_STAR;
            play_sound(SOUND_MARIO_OOOF2, m->marioObj->header.gfx.cameraToObject);
            return set_mario_action(m, ACT_BACKWARD_WATER_KB, 0);

        default:
            if ((get_cappy_action_timer() % 4) == 0) {
                play_sound(SOUND_ACTION_UNKNOWN434, m->marioObj->header.gfx.cameraToObject);
            }
            break;
    }
    return FALSE;
}

s32 act_water_descent(struct MarioState *m) {

    if (m->actionState == 0) {

        if (m->actionTimer < 10) {
            m->pos[1] += (20 - 2 * m->actionTimer);
            m->peakHeight = m->pos[1];
            vec3f_copy(m->marioObj->header.gfx.pos, m->pos);
        }

        m->vel[0] = 0;
        m->vel[1] = 0;
        m->vel[2] = 0;

        // Water dash
        if (m->input & INPUT_B_PRESSED) {
            return set_mario_action(m, ACT_WATER_DASH_START, 0);
        }

        set_mario_animation(m, MARIO_ANIM_START_GROUND_POUND);
        if (m->actionTimer == 0) {
            play_sound(SOUND_ACTION_SPIN, m->marioObj->header.gfx.cameraToObject);
        }

        if (++(m->actionTimer) >= m->marioObj->header.gfx.unk38.curAnim->unk08 + 4) {
            play_sound(SOUND_MARIO_GROUND_POUND_WAH, m->marioObj->header.gfx.cameraToObject);
            play_sound(SOUND_ACTION_UNKNOWN430, m->marioObj->header.gfx.cameraToObject);
            decrease_O2_level(m, 60);
            m->actionState = 1;
            m->vel[1] = -105.f;
            m->faceAngle[0] = 0;
            m->faceAngle[2] = 0;
        }

    } else {

        set_mario_animation(m, MARIO_ANIM_GROUND_POUND);
        m->particleFlags |= (PARTICLE_PLUNGE_BUBBLE | PARTICLE_BUBBLE);
        m->vel[1] += 5.0f;

        if (m->vel[1] >= 0) {
            m->vel[1] = 0;
            return set_mario_action(m, ACT_WATER_IDLE, 0);
        }

        u32 stepResult = perform_water_step(m);
        if (stepResult == WATER_STEP_HIT_FLOOR) {
            return set_mario_action(m, ACT_WATER_IDLE, 0);
        }
    }
    return FALSE;
}

void init_metal_water_cappy_throw(u8 state) {
    init_cappy_action_timer(10);
    init_cappy_action_state(state);
}

s32 act_metal_water_cappy_throw(struct MarioState *m) {

    if (decrease_cappy_action_timer()) {
        if (get_cappy_action_state()) {
            return set_mario_action(m, ACT_METAL_WATER_FALLING, 0);
        } else {
            return set_mario_action(m, ACT_METAL_WATER_STANDING, 0);
        }
    }

    if (get_cappy_action_timer() == 9) {
        if (get_cappy_action_state() == 1) {
            play_sound(SOUND_MARIO_PUNCH_HOO, m->marioObj->header.gfx.cameraToObject);
            m->marioObj->header.gfx.unk38.animID = -1;
            set_mario_animation(m, MARIO_ANIM_AIR_KICK);
        } else {
            play_sound(SOUND_MARIO_PUNCH_YAH, m->marioObj->header.gfx.cameraToObject);
            m->marioObj->header.gfx.unk38.animID = -1;
            set_mario_animation(m, MARIO_ANIM_FIRST_PUNCH);
        }
    }

    if (perform_water_step(m) == WATER_STEP_HIT_FLOOR) {
        return set_mario_action(m, ACT_METAL_WATER_STANDING, 0);
    }
    return FALSE;
}

#define UNDERWATER_BREATH_MAX_DURATION  1800    // 60 seconds, 20 seconds in cold water
#define UNDERWATER_ABOUT_TO_DROWN       1350    // 45 seconds, 15 seconds in cold water

void reset_O2_level(struct MarioState *m) {
    if (m->unk00 != 0) {
        play_sound(SOUND_MENU_POWER_METER, m->marioObj->header.gfx.cameraToObject);
    }
    m->unk00 = 0;
}

void increase_O2_level(struct MarioState *m, u16 amount) {
    if (m->unk00 != 0) {
        play_sound(SOUND_MENU_POWER_METER, m->marioObj->header.gfx.cameraToObject);
    }
    if (m->unk00 < amount) {
        m->unk00 = 0;
    } else {
        m->unk00 -= amount;
    }
}

void decrease_O2_level(struct MarioState *m, u16 amount) {
    m->unk00 += amount;
}

void display_O2_level(struct MarioState *m) {
    s32 level = max((((s32)(UNDERWATER_BREATH_MAX_DURATION - m->unk00) * 100) / UNDERWATER_BREATH_MAX_DURATION), 0);
    char buff[16]; sprintf(buff, "%03d", level);
    print_text(GFX_DIMENSIONS_RECT_FROM_LEFT_EDGE(22), 185, "2");
    print_text(GFX_DIMENSIONS_RECT_FROM_LEFT_EDGE(13), 191, "O");
    print_text(GFX_DIMENSIONS_RECT_FROM_LEFT_EDGE(38), 191, "*");
    print_text(GFX_DIMENSIONS_RECT_FROM_LEFT_EDGE(54), 191, buff);
}

s32 is_mario_about_to_drown(struct MarioState *m) {
    return (m->unk00 >= UNDERWATER_ABOUT_TO_DROWN);
}

s32 is_mario_drowning(struct MarioState *m) {
    return (m->unk00 >= UNDERWATER_BREATH_MAX_DURATION);
}
