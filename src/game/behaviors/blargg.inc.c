#include <stdbool.h>
#include "engine/math_util.h";

// bully.c.inc

static struct ObjectHitbox sBlaargHitbox = {
    /* interactType:      */ INTERACT_DAMAGE,
    /* downOffset:        */ 0,
    /* damageOrCoinValue: */ 1,
    /* health:            */ 0,
    /* numLootCoins:      */ 0,
    /* radius:            */ 300,
    /* height:            */ 235,
    /* hurtboxRadius:     */ 300,
    /* hurtboxHeight:     */ 110,
};

void bhv_friendly_blargg_init(void) {
    cur_obj_init_animation(0);
    // obj_set_hitbox(o, &sBlaargHitbox);
    o->oHomeX = o->oPosX;
    o->oHomeY = o->oPosY;
    o->oHomeZ = o->oPosZ;
}

void bhv_blargg_init(void) {
    cur_obj_init_animation(0);

    o->oHomeX = o->oPosX;
    o->oHomeZ = o->oPosZ;
    o->oGravity = 4.0;
    o->oFriction = 0.91;
    o->oBuoyancy = 1.3;

    obj_set_hitbox(o, &sBlaargHitbox);
}

void blargg_check_mario_collision(void) {
    if (o->oInteractStatus & INT_STATUS_INTERACTED) {
        cur_obj_play_sound_2(SOUND_OBJ2_BULLY_ATTACKED);

        o->oInteractStatus &= ~INT_STATUS_INTERACTED;
        o->oAction = BULLY_ACT_KNOCKBACK;
        o->oFlags &= ~0x8; /* bit 3 */
        cur_obj_init_animation(0);
        o->oBullyMarioCollisionAngle = o->oMoveAngleYaw;
    }
}

f32 get_distance_from_mario(){
    Vec3f marioPos;
    marioPos[0] = gMarioObject->oPosX;
    marioPos[1] = gMarioObject->oPosY;
    marioPos[2] = gMarioObject->oPosZ;

    Vec3f objPos;

    objPos[0] = o->oPosX;
    objPos[1] = o->oPosY;
    objPos[2] = o->oPosZ;

    f32 dist;
    s16 pitch;
    s16 yaw;

    vec3f_get_dist_and_angle(objPos, marioPos, &dist, &pitch, &yaw);

    return dist;
}

void blargg_act_chase_mario(void) {
    f32 homeX = o->oHomeX;
    f32 posY = o->oPosY;
    f32 homeZ = o->oHomeZ;

    o->oFlags |= 0x8;
    o->oMoveAngleYaw = o->oFaceAngleYaw;
    obj_turn_toward_object(o, gMarioObject, 16, 4096);

    o->oForwardVel = (gMarioState->riddenObj == NULL) ? 10 : 40;

    if (o->oTimer < 10) {
        // o->oForwardVel = (gMarioState->riddenObj != NULL) ? 40.0 : 8.0;
    } else {

        if (o->oTimer >= 31)
            o->oTimer = 0;
    }

    f32 distance = get_distance_from_mario();

    if(distance < 1600){
        //o->oForwardVel = (gMarioState->riddenObj == NULL) ? distance / 40 : 50;
    }

    if(distance > 500){
        if(cur_obj_check_if_at_animation_end() == 1){
            cur_obj_init_animation(0);
        }        
        bhv_koopa_shell_flame_spawn();
    }else{
        if(cur_obj_check_if_at_animation_end() == 1){
            // o->oForwardVel = (gMarioState->riddenObj != NULL) ? 50.0 : 20.0;
            cur_obj_init_animation(1);
        }
    }

    struct Surface *sp34 = cur_obj_update_floor_height_and_get_floor();        

    printf("Dist: %f\n", distance);

    s32 radius = is_point_within_radius_of_mario(homeX, posY, homeZ, 3000);

    if (distance > 1500 || gMarioState->floor->type == 0 || sp34->type != 1) {
        o->oAction = BULLY_ACT_PATROL;
        cur_obj_init_animation(0);
    }
}

void blargg_act_knockback(void) {
    if (o->oForwardVel < 10.0 && (s32) o->oVelY == 0) {
        o->oForwardVel = 1.0;
        o->oBullyKBTimerAndMinionKOCounter++;
        o->oFlags |= 0x8; /* bit 3 */
        o->oMoveAngleYaw = o->oFaceAngleYaw;
        obj_turn_toward_object(o, gMarioObject, 16, 1280);
    } else
        o->header.gfx.unk38.animFrame = 0;

    if (o->oBullyKBTimerAndMinionKOCounter == 18) {
        o->oAction = BULLY_ACT_CHASE_MARIO;
        o->oBullyKBTimerAndMinionKOCounter = 0;
        cur_obj_init_animation(1);
    }
}

void blargg_act_back_up(void) {
    if (o->oTimer == 0) {
        o->oFlags &= ~0x8; /* bit 3 */
        o->oMoveAngleYaw += 0x8000;
    }    

    o->oForwardVel = 5.0;    

    //! bully_backup_check() happens after this function, and has the potential to reset
    //  the bully's action to BULLY_ACT_BACK_UP. Because the back up action is only
    //  set to end when the timer EQUALS 15, if this happens on that frame, the bully
    //  will be stuck in BULLY_ACT_BACK_UP forever until Mario hits it or its death
    //  conditions are activated. However because its angle is set to its facing angle,
    //  it will walk forward instead of backing up.

    if (o->oTimer == 15) {
        o->oMoveAngleYaw = o->oFaceAngleYaw;
        o->oFlags |= 0x8; /* bit 3 */
        o->oAction = BULLY_ACT_PATROL;
    }
}

void blargg_backup_check(s16 collisionFlags) {
    if (!(collisionFlags & 0x8) && o->oAction != BULLY_ACT_KNOCKBACK) /* bit 3 */
    {
        o->oPosX = o->oBullyPrevX;
        o->oPosZ = o->oBullyPrevZ;
        o->oAction = BULLY_ACT_BACK_UP;
    }
}

void blargg_play_stomping_sound(void) {
    s16 sp26 = o->header.gfx.unk38.animFrame;
    switch (o->oAction) {
        case BULLY_ACT_PATROL:
            if (sp26 == 0 || sp26 == 12) {
                cur_obj_play_sound_2(SOUND_OBJ_BULLY_WALK);
            }
            break;

        case BULLY_ACT_CHASE_MARIO:
        case BULLY_ACT_BACK_UP:
            if (sp26 == 0 || sp26 == 5) {
                cur_obj_play_sound_2(SOUND_OBJ_BULLY_WALK);
            }
            break;
    }
}

void blargg_step(void) {
    s16 collisionFlags = 0;
    collisionFlags = object_step();
    blargg_backup_check(collisionFlags);
    blargg_play_stomping_sound();
}

void bhv_blargg_loop(void) {
    o->oBullyPrevX = o->oPosX;
    o->oBullyPrevY = o->oPosY;
    o->oBullyPrevZ = o->oPosZ;

    //! Because this function runs no matter what, Mario is able to interrupt the bully's
    //  death action by colliding with it. Since the bully hitbox is tall enough to collide
    //  with Mario even when it is under a lava floor, this can get the bully stuck OOB
    //  if there is nothing under the lava floor.
    blargg_check_mario_collision();

    switch (o->oAction) {
        case BULLY_ACT_PATROL:
            o->oForwardVel = 5.0;

            f32 distance = get_distance_from_mario();

            if (distance < 1000 && gMarioState->floor->type != 0) {
                o->oAction = BULLY_ACT_CHASE_MARIO;
            }

            blargg_step();

            break;

        case BULLY_ACT_CHASE_MARIO:
            blargg_act_chase_mario();
            blargg_step();
            break;

        case BULLY_ACT_KNOCKBACK:
            blargg_act_knockback();
            blargg_step();
            break;

        case BULLY_ACT_BACK_UP:
            o->oForwardVel = 10.0;
            blargg_act_back_up();
            blargg_step();
            break;

        case BULLY_ACT_DEATH_PLANE_DEATH:
            o->activeFlags = 0;
            break;
    }

    set_object_visibility(o, 3000);
}

struct ObjectHitbox sBlarggFriendlyHitbox = {
    /* interactType: */ INTERACT_KOOPA_SHELL,
    /* downOffset: */ 0,
    /* damageOrCoinValue: */ 4,
    /* health: */ 1,
    /* numLootCoins: */ 1,
    /* radius: */ 50,
    /* height: */ 50,
    /* hurtboxRadius: */ 50,
    /* hurtboxHeight: */ 50,
};


void blargg_respawn(void) {
    gMarioState->action = ACT_WALKING;
    mario_stop_riding_object(gMarioState);

    obj_mark_for_deletion(o);
    struct Object *explosion;
    if (o->oTimer < 5)
        cur_obj_scale(1.0 + (f32) o->oTimer / 5.0);
    else {
        explosion = spawn_object(o, MODEL_EXPLOSION, bhvExplosion);
        explosion->oGraphYOffset += 100.0f;

        create_respawner(MODEL_FRIENDLY_BLARGG, bhvFriendlyBlargg, 4000);
        o->activeFlags = 0;
    }
}

void bhv_blargg_friendly_loop(void) {
    struct Surface *sp34;
    obj_set_hitbox(o, &sBlarggFriendlyHitbox);
    cur_obj_scale(1.0f);
    switch (o->oAction) {
        case 0:
            cur_obj_update_floor_and_walls();
            cur_obj_if_hit_wall_bounce_away();
            if (o->oInteractStatus & INT_STATUS_INTERACTED)
                o->oAction++;            
            cur_obj_move_standard(-20);
            break;
        case 1:
            obj_copy_pos(o, gMarioObject);
            sp34 = cur_obj_update_floor_height_and_get_floor();
            if (5.0f > absf(o->oPosY - o->oFloorHeight)) {
                if (sp34 != NULL && sp34->type == 1)
                    bhv_koopa_shell_flame_spawn();
                else
                    blargg_respawn();
             } else
                blargg_respawn();
            o->oFaceAngleYaw = gMarioObject->oMoveAngleYaw;
            if (o->oInteractStatus & INT_STATUS_STOP_RIDING) {
                obj_mark_for_deletion(o);
                spawn_mist_particles();
                o->oAction = 0;
            }
            break;
    }
    o->oInteractStatus = 0;
}