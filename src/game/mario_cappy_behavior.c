#include "mario_cappy.h"
#include "behavior_data.h"
#include "sm64.h"
#include "object_list_processor.h"
#include "object_helpers.h"
#include "level_update.h"
#include "engine/surface_collision.h"
#include "interaction.h"
#include "engine/behavior_script.h"
#include "engine/math_util.h"
#include "audio/external.h"
#include "pc/cheats.h"
#include "pc/configfile.h"
#include "mario.h"

#define CAPPY_HITBOX_RADIUS           96.f
#define CAPPY_HITBOX_HEIGHT           112.f
#define CAPPY_HITBOX_DOWNOFFSET       32.f
#define CAPPY_MAX_DISTANCE_GROUND     840.f
#define CAPPY_MAX_DISTANCE_AIR        1260.f
#define CAPPY_MAX_DISTANCE_WATER      1080.f
#define CAPPY_MAX_DISTANCE_METALWATER 840.f
#define CAPPY_MAX_DISTANCE_DURATION   12
#define CAPPY_LIFETIME                132
#define CAPPY_HITBOX_ACTIVE_START     ((CAPPY_LIFETIME) - (CAPPY_MAX_DISTANCE_DURATION) - 1)

static s16 gCappyYaw = 0;
static u8 gCappyTimer = 0;
static u8 gCappyJumped = FALSE;
static u8 gActionTimer = 0;
static u8 gActionState = 0;

// List of enemies attackable by Cappy (behaviors)
static const BehaviorScript* gAttackableBehaviors[] = {
    bhvBlargg,
    bhvMrI,
    bhvChuckya,
    bhvThwomp,
    bhvThwomp2,
    bhvSmallWhomp,
    bhvPiranhaPlant,
    bhvBobomb,
    bhvBobombBuddy,
    bhvBoo,
    bhvBooInCastle,
    bhvBooWithCage,
    bhvBalconyBigBoo,
    bhvMerryGoRoundBigBoo,
    bhvGhostHuntBigBoo,
    bhvMerryGoRoundBoo,
    bhvGhostHuntBoo,
    bhvHauntedBookshelf,
    bhvScuttlebug,
    bhvMoneybag,
    bhvMoneybagHidden,
    bhvKoopa,
    bhvPokey,
    bhvPokeyBodyPart,
    bhvSwoop,
    bhvFlyGuy,
    bhvGoomba,
    bhvEnemyLakitu,
    bhvSpiny,
    bhvMontyMole,
    bhvFlyingBookend,
    bhvFirePiranhaPlant,
    bhvSnufit,
    bhvKlepto,
    bhvSkeeter,
    bhvSmallBully,
    bhvBigBully,
    bhvBigBullyWithMinions,
    bhvSmallChillBully,
    bhvBigChillBully
};
static const u32 gAttackableCount = sizeof(gAttackableBehaviors) / sizeof(const BehaviorScript*);

// Bullies
static const BehaviorScript* gBullyBehaviors[] = {
    bhvSmallBully,
    bhvBigBully,
    bhvBigBullyWithMinions,
    bhvSmallChillBully,
    bhvBigChillBully
};
static const u32 gBullyCount = sizeof(gBullyBehaviors) / sizeof(const BehaviorScript*);

//
// Collision handler
//

static s32 cappy_detect_hitbox_overlap(struct Object *cap, struct Object *obj) {

    // Radius check
    f32 dx = cap->oPosX - obj->oPosX;
    f32 dz = cap->oPosZ - obj->oPosZ;
    f32 collisionRadius2 = (CAPPY_HITBOX_RADIUS + obj->hitboxRadius) * (CAPPY_HITBOX_RADIUS + obj->hitboxRadius);
    f32 distance2 = (dx * dx + dz * dz);
    if (distance2 > collisionRadius2) {
        return FALSE;
    }

    // Height check
    f32 capLowerBound = cap->oPosY - CAPPY_HITBOX_DOWNOFFSET;
    f32 capUpperBound = CAPPY_HITBOX_HEIGHT + capLowerBound;
    f32 objLowerBound = obj->oPosY - obj->hitboxDownOffset;
    f32 objUpperBound = obj->hitboxHeight + objLowerBound;
    f32 sumOfHeights = CAPPY_HITBOX_HEIGHT + obj->hitboxHeight;
    if (((objUpperBound - capLowerBound) > sumOfHeights) ||
        ((capUpperBound - objLowerBound) > sumOfHeights)) {
        return FALSE;
    }

    return TRUE;
}

static s32 is_mario(struct Object *obj) {
    return (gMarioState->marioObj == obj);
}

static s32 is_coin_or_secret(struct Object *obj) {

    // Coin
    if (obj->oInteractType & INTERACT_COIN) {
        return TRUE;
    }

    // Star secret
    if (obj->behavior == bhvHiddenStarTrigger) {
        return TRUE;
    }

    return FALSE;
}

static s32 is_grabbable(struct Object *obj) {

    // Not Bowser
    if (obj->behavior == bhvBowser) {
        return FALSE;
    }

    // Not that @#%! hat stealer ukiki
    if (obj->behavior == bhvUkiki && obj->oBehParams2ndByte == UKIKI_HAT) {
        return FALSE;
    }
    if (obj->behavior == bhvMacroUkiki && obj->oBehParams2ndByte == UKIKI_HAT) {
        return FALSE;
    }

    // Not Heave ho
    if (obj->oInteractionSubtype & INT_SUBTYPE_NOT_GRABBABLE) {
        return FALSE;
    }

    // Grabbable
    if (obj->oInteractType & INTERACT_GRABBABLE) {
        return TRUE;
    }

    return FALSE;
}

static s32 is_attackable(struct Object *obj) {
    for (u32 i = 0; i != gAttackableCount; ++i) {
        if (obj->behavior == gAttackableBehaviors[i]) {
            return TRUE;
        }
    }
    return FALSE;
}

static s32 is_bully(struct Object *obj) {
    for (u32 i = 0; i != gBullyCount; ++i) {
        if (obj->behavior == gBullyBehaviors[i]) {
            return TRUE;
        }
    }
    return FALSE;
}

static s32 cappy_interact_with_mario(struct MarioState *m, struct Object *cap) {
    // gMarioState->marioObj->header.gfx.sharedChild = gLoadedGraphNodes[MODEL_WARIO];

    // Underwater (non Metal) Mario can't interact with Cappy
    if (m->action & ACT_FLAG_SWIMMING) {
        return FALSE;
    }

    // Cappy is not tangible yet for Mario
    if (gCappyTimer > CAPPY_HITBOX_ACTIVE_START) {
        return FALSE;
    }

    // Counts as a Cappy jump only
    // if Mario is airborne
    if (m->action & ACT_FLAG_AIR) {
        gCappyJumped = TRUE;
    }

    if (m->action & ACT_FLAG_METAL_WATER) {
        set_mario_action(m, ACT_METAL_WATER_JUMP, 0);
    } else {
        set_mario_action(m, ACT_DOUBLE_JUMP, 0);
    }

    spawn_object(m->marioObj, MODEL_NONE, bhvHorStarParticleSpawner);
    play_sound(SOUND_GENERAL_BOING1, m->marioObj->header.gfx.cameraToObject);
    obj_mark_for_deletion(cap);

    if (m->flags & MARIO_CAP_THROWN) {
        m->flags |= MARIO_CAP_ON_HEAD;
        m->flags &= ~MARIO_CAP_THROWN;
    }

    return TRUE;
}

static void cappy_give_collectible_to_mario(struct Object *obj) {

    // Teleports collectible directly to Mario
    // The object is then collected on the very next frame
    obj->oPosX = gMarioObject->oPosX;
    obj->oPosY = gMarioObject->oPosY + 80;
    obj->oPosZ = gMarioObject->oPosZ;
}

static s32 cappy_try_to_grab_object(struct Object *cap, struct Object *obj) {

    // Even if an object is grabbable by Cappy,
    // the actual grab is performed only by grounded Mario
    if (!(gMarioState->action & (ACT_FLAG_AIR | ACT_FLAG_SWIMMING | ACT_FLAG_METAL_WATER))) {
        gMarioState->input |= INPUT_INTERACT_OBJ_GRABBABLE;
        gMarioState->interactObj = obj;
        gMarioState->usedObj = obj;
        gMarioState->vel[0] = 0;
        gMarioState->vel[1] = 0;
        gMarioState->vel[2] = 0;
        set_mario_animation(gMarioState, MARIO_ANIM_FIRST_PUNCH);
        set_mario_action(gMarioState, ACT_PICKING_UP, 0);


        if (gMarioState->flags & MARIO_CAP_THROWN) {
            gMarioState->flags |= MARIO_CAP_ON_HEAD;
            gMarioState->flags &= ~MARIO_CAP_THROWN;
        }

        obj_mark_for_deletion(cap);
        return TRUE;
    }
    return FALSE;
}

static void cappy_attack_and_disappear_on_contact(struct Object *cap, struct Object *obj) {

    gMarioState->pos[0] = obj->oPosX;
    gMarioState->pos[1] = obj->oPosY;
    gMarioState->pos[2] = obj->oPosZ;


    // Attack
    if (is_bully(obj)) {
        obj->oMoveAngleYaw = gCappyYaw;
        obj->oForwardVel = 3392.0f / obj->hitboxRadius;

        obj->oInteractStatus = (0x02 | INT_STATUS_INTERACTED | INT_STATUS_WAS_ATTACKED);
    } else {
        configModelPrev = configModel;
        configModel = 4;
        obj_mark_for_deletion(obj);
    }

    // Spawn white mist
    for (s32 i = 0; i < 16; ++i) {
        struct Object *particle = spawn_object(cap, MODEL_MIST, bhvWhitePuffExplosion);
        if (particle != NULL) {
            particle->oBehParams2ndByte = 2;
            particle->oMoveAngleYaw = random_u16();
            particle->oGravity = -4.f;
            particle->oDragStrength = 30.f;
            particle->oForwardVel = random_float() * 5.f + 40.f;
            particle->oVelY = random_float() * 20.f + 30.f;
            f32 scale = random_float() * 0.25f + 4.6f;
            obj_scale_xyz(particle, scale, scale, scale);
        }
    }

    // Sound effect
    struct Object *sound = spawn_object(cap, 0, bhvSoundSpawner);
    if (sound != NULL) {
        sound->oSoundEffectUnkF4 = SOUND_OBJ_DEFAULT_DEATH;
    }

    if (gMarioState->flags & MARIO_CAP_THROWN) {
        gMarioState->flags |= MARIO_CAP_ON_HEAD;
        gMarioState->flags &= ~MARIO_CAP_THROWN;
    }


    
    if (!is_bully(obj)) {
        gMarioState->marioObj->header.gfx.sharedChild = gLoadedGraphNodes[obj->unused1];
    }


    obj_mark_for_deletion(cap);
}

static s32 cappy_process_collision_in_list(struct Object *cap, struct Object *obj,
                                           struct Object *first) {
    while (obj != NULL && obj != first) {
        if (obj != cap && obj->oIntangibleTimer == 0 && cappy_detect_hitbox_overlap(cap, obj)) {

            // Mario
            if (is_mario(obj)) {
                if (cappy_interact_with_mario(gMarioState, cap)) {
                    return TRUE;
                }
            }

            // Collectible
            else if (is_coin_or_secret(obj)) {
                cappy_give_collectible_to_mario(obj);
            }

            // Grabbable
            // else if (is_grabbable(obj)) {
            //     if (cappy_try_to_grab_object(cap, obj)) {
            //         return TRUE;
            //     }
            // }

            // Attackable
            else if (is_attackable(obj) || is_grabbable(obj)) {
                cappy_attack_and_disappear_on_contact(cap, obj);
                return TRUE;
            }
        }
        obj = (struct Object*) obj->header.next;
    }
    return FALSE;
}

static void cappy_process_object_collision(struct Object *cap) {

    // OBJ_LIST_PLAYER
    // OBJ_LIST_UNUSED_1
    // OBJ_LIST_DESTRUCTIVE
    // OBJ_LIST_UNUSED_3
    // OBJ_LIST_GENACTOR
    // OBJ_LIST_PUSHABLE
    // OBJ_LIST_LEVEL
    for (u32 type = OBJ_LIST_PLAYER; type <= OBJ_LIST_LEVEL; ++type) {

        // Break the loop if something happens (cappy jump, grab, enemy hit)
        if (cappy_process_collision_in_list(cap, (struct Object*) gObjectLists[type].next, (struct Object*) &gObjectLists[type])) {
            break;
        }
    }
}

//
// Movement
//

static void cappy_move_xyz(struct Object *cap) {

    cap->oPosX += cap->oVelX;
    cap->oPosY += cap->oVelY;
    cap->oPosZ += cap->oVelZ;

    // Wall detection
    struct WallCollisionData hitbox;
    hitbox.x = cap->oPosX;
    hitbox.y = cap->oPosY;
    hitbox.z = cap->oPosZ;
    hitbox.offsetY = CAPPY_HITBOX_HEIGHT / 2;
    hitbox.radius = CAPPY_HITBOX_RADIUS;
    if (find_wall_collisions(&hitbox) != 0) {
        cap->oPosX = hitbox.x;
        cap->oPosY = hitbox.y;
        cap->oPosZ = hitbox.z;
        cap->oVelX = 0;
        cap->oVelY = 0;
        cap->oVelZ = 0;
    }

    // Floor distance
    struct Surface *sObjFloor = NULL;
    f32 floorY = find_floor(cap->oPosX, cap->oPosY, cap->oPosZ, &sObjFloor);
    f32 diffY = cap->oPosY - floorY;
    if (diffY < 0) {
        cap->oPosY = floorY;
        cap->oVelY = 0;
    }
}

//
// Behavior
//

static void bhv_cappy_loop(void) {
    if (--gCappyTimer == 0) {
        obj_mark_for_deletion(gCurrentObject);

        if (gMarioState->flags & MARIO_CAP_THROWN) {
            gMarioState->flags |= MARIO_CAP_ON_HEAD;
            gMarioState->flags &= ~MARIO_CAP_THROWN;
        }


        return;
    }

    if (gCappyTimer > CAPPY_HITBOX_ACTIVE_START) {
        cappy_move_xyz(gCurrentObject);
    }

    cappy_process_object_collision(gCurrentObject);
    spawn_object(gCurrentObject, MODEL_NONE, bhvSparkleSpawn);
    gCurrentObject->oFaceAngleYaw += 0x2000;
    gCurrentObject->oOpacity = ((gMarioState->flags & MARIO_VANISH_CAP) ? 0x80 : 0xFF);
}

//
// Actions
//

// const BehaviorScript bhvCappy[] = {
//     BEGIN(OBJ_LIST_LEVEL),
//     OR_INT(oFlags, (OBJ_FLAG_COMPUTE_DIST_TO_MARIO | OBJ_FLAG_UPDATE_GFX_POS_AND_ANGLE)),
//     BEGIN_LOOP(),
//         CALL_NATIVE(bhv_cappy_loop),
//     END_LOOP(),
// };
static const uintptr_t bhvCappy[] = {
    0x00060000,
    0x11010041,
    0x08000000,
    0x0C000000,
    (uintptr_t)(bhv_cappy_loop),
    0x09000000
};

s32 spawn_cappy(struct MarioState *m, u8 flag) {
    configModel = configModelPrev;

    // If Mario already Cappy-jumped or lost his hat,
    // he can't throw it again
    if (gCappyJumped || (!(m->flags & MARIO_CAP_THROWN) && !(m->flags & MARIO_CAP_ON_HEAD))) {
        return FALSE;
    }

    // There can be only one instance of Cappy at a time
    // so the previous one must be deleted beforehand
    struct Object *obj = (struct Object *) gObjectLists[OBJ_LIST_LEVEL].next;
    struct Object *first = (struct Object *) &gObjectLists[OBJ_LIST_LEVEL];
    while (obj != NULL && obj != first) {
        if (obj->behavior == bhvCappy) {
            obj_mark_for_deletion(obj);
            break;
        }
        obj = (struct Object *) obj->header.next;
    }

    // Spawn Cappy according to the current cap
    struct Object *cappy = NULL;
    if (m->flags & MARIO_METAL_CAP) {
        u32 model = MODEL_MARIOS_METAL_CAP;

        if(Cheats.EnableCheats) {
            if(configModel == 1) {
                model = MODEL_LUIGIS_METAL_CAP;
            }else if(configModel == 2) {
                model = MODEL_WARIOS_METAL_CAP;
            }
        }

        cappy = spawn_object(m->marioObj, model, bhvCappy);
    } else if (m->flags & MARIO_VANISH_CAP) {
        u32 model = MODEL_MARIOS_CAP;

        if(Cheats.EnableCheats) {
            if(configModel == 1) {
                model = MODEL_LUIGIS_CAP;
            }else if(configModel == 2) {
                model = MODEL_WARIOS_CAP;
            }
        }

        cappy = spawn_object(m->marioObj, model, bhvCappy);
    } else if (m->flags & MARIO_WING_CAP) {
        u32 model = MODEL_MARIOS_WING_CAP;

        if(Cheats.EnableCheats) {
            if(configModel == 1) {
                model = MODEL_LUIGIS_WING_CAP;
            }else if(configModel == 2) {
                model = MODEL_WARIOS_WING_CAP;
            }
        }
        cappy = spawn_object(m->marioObj, model, bhvCappy);
    } else {
        u32 model = MODEL_MARIOS_CAP;

        if(Cheats.EnableCheats) {
            if(configModel == 1) {
                model = MODEL_LUIGIS_CAP;
            }else if(configModel == 2) {
                model = MODEL_WARIOS_CAP;
            }
        }

        cappy = spawn_object(m->marioObj, model, bhvCappy);
    }

    // If, for whatever reason, the game fails
    // to spawn Cappy, better handle this
    if (cappy == NULL) {
        return FALSE;
    }

    if (m->flags & MARIO_CAP_ON_HEAD) {
        m->flags |= MARIO_CAP_THROWN;
        m->flags &= ~MARIO_CAP_ON_HEAD;
    }

    // Parameters
    s16 mPitch, mYaw;
    f32 hVel, vVel, yOffset;
    switch (flag) {
        case CAPPY_FLAG_METALWATER:
            hVel = (CAPPY_MAX_DISTANCE_METALWATER / CAPPY_MAX_DISTANCE_DURATION);
            vVel = 0;
            mPitch = 0;
            mYaw = m->faceAngle[1];
            yOffset = 50.f;
            break;

        case CAPPY_FLAG_UNDERWATER:
            hVel = (CAPPY_MAX_DISTANCE_WATER / CAPPY_MAX_DISTANCE_DURATION);
            vVel = (CAPPY_MAX_DISTANCE_WATER / CAPPY_MAX_DISTANCE_DURATION);
            mPitch = m->faceAngle[0];
            mYaw = m->faceAngle[1];
            yOffset = 0;
            break;

        case CAPPY_FLAG_AIRBORNE:
            hVel = (CAPPY_MAX_DISTANCE_AIR / CAPPY_MAX_DISTANCE_DURATION);
            vVel = 0;
            mPitch = 0;
            mYaw = m->faceAngle[1];
            yOffset = 50;
            break;

        default:
            hVel = (CAPPY_MAX_DISTANCE_GROUND / CAPPY_MAX_DISTANCE_DURATION);
            vVel = 0;
            mPitch = 0;
            mYaw = m->faceAngle[1];
            yOffset = 50;
            break;
    }

    cappy->oVelX = coss(mPitch) * sins(mYaw) * hVel;
    cappy->oVelY = sins(mPitch) * vVel;
    cappy->oVelZ = coss(mPitch) * coss(mYaw) * hVel;
    cappy->oPosX = m->pos[0];
    cappy->oPosY = m->pos[1] + yOffset;
    cappy->oPosZ = m->pos[2];
    cappy->oFaceAngleYaw = 0;
    cappy->oFaceAnglePitch = 0;
    cappy->oFaceAngleRoll = 0;
    gCappyYaw = mYaw;
    gCappyTimer = CAPPY_LIFETIME;

    return TRUE;
}

void reset_cappy_jump() {
    gCappyJumped = FALSE;
}

void init_cappy_action_timer(u8 duration) {
    gActionTimer = duration;
}

void init_cappy_action_state(u8 state) {
    gActionState = state;
}

// Returns TRUE if gActionTimer is 0
s32 decrease_cappy_action_timer() {
    if (gActionTimer > 0) {
        gActionTimer--;
    }
    return (gActionTimer == 0);
}

u8 get_cappy_action_timer() {
    return gActionTimer;
}

u8 get_cappy_action_state() {
    return gActionState;
}
