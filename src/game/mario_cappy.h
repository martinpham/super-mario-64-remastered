#ifndef MARIO_CAPPY_H
#define MARIO_CAPPY_H

#include "sm64.h"

#define CAPPY_FLAG_GROUND		0
#define CAPPY_FLAG_AIRBORNE		1
#define CAPPY_FLAG_UNDERWATER	2
#define CAPPY_FLAG_METALWATER	3

// Actions //
s32 spawn_cappy(struct MarioState *m, u8 flag);
void reset_cappy_jump();
void init_cappy_action_timer(u8 duration);
void init_cappy_action_state(u8 state);
s32 decrease_cappy_action_timer();
u8 get_cappy_action_timer();
u8 get_cappy_action_state();

// Ground //
s32 act_rolling_start(struct MarioState *m);
s32 act_rolling(struct MarioState *m);

// Airborne //
void reset_wall_slide();
s32 mario_check_wall_slide(struct MarioState *m);
s32 mario_start_wall_slide(struct MarioState *m);
s32 act_wall_slide(struct MarioState *m);
s32 act_dive_start(struct MarioState *m);

// Underwater //
s32 act_water_dash_start(struct MarioState *m);
s32 act_water_dash(struct MarioState *m);
s32 act_water_descent(struct MarioState *m);
s32 act_metal_water_cappy_throw(struct MarioState *m);
void init_metal_water_cappy_throw(u8 state);

// O2 level //
void reset_O2_level(struct MarioState *m);
void increase_O2_level(struct MarioState *m, u16 amount);
void decrease_O2_level(struct MarioState *m, u16 amount);
void display_O2_level(struct MarioState *m);
s32 is_mario_about_to_drown(struct MarioState *m);
s32 is_mario_drowning(struct MarioState *m);

#endif // MARIO_CAPPY_H
