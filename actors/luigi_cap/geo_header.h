extern const GeoLayout luigi_cap_001_switch_opt1[];
extern const GeoLayout luigi_cap_geo[];
extern Vtx luigi_cap_000_displaylist_mesh_vtx_0[5580];
extern Gfx luigi_cap_000_displaylist_mesh_tri_0[];
extern Vtx luigi_cap_000_displaylist_mesh_vtx_1[151];
extern Gfx luigi_cap_000_displaylist_mesh_tri_1[];
extern Vtx luigi_cap_000_displaylist_mesh_vtx_2[663];
extern Gfx luigi_cap_000_displaylist_mesh_tri_2[];

extern Gfx luigi_capbase[];
extern Gfx luigi_capwings[];
extern Gfx luigi_capmetal[];
extern Gfx luigi_capmetalwings[];
extern Gfx luigi_cap_material_revert_render_settings[];