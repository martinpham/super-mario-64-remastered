# sm64ex
Fork of sm64ex with additional features. Video demo [https://youtu.be/ruY8SJB9Rdg](https://youtu.be/ruY8SJB9Rdg).

- 60fps & Blargg prepatched
- Textures from sm64redrawn & T. Rexxer
- Models from SGI
- Super Mario Odyssey moveset from PeachyPeach & cappy patched
- Non stop patch from GateGuy
- Cap cheats
- Koopa Troopa shell cheat
- Wario playable model
- Luigi playable model from Arredondo

## Quick build

For the first build, you should build, clean, and build again to apply textures correctly.

### MacOS
- Install Homebrew

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`

- Install dependencies

`./tools.sh`

- Place `SuperMario64.z64` ROM as `baserom.us.z64`


- Build

`./build.sh`

- *or Clean build*

`./clean-build.sh`

- Run

`./run.sh`

- All in one

`./start.sh`

## Keymap
- **Start**: `Space`
- **Up/Left/Down/Right**: `W/A/S/D`
- **Right shoulder**: `Right Shift`
- **Left shoulder**: `K`
- **A**: `L`
- **B**: `,`


-----

## General

Feel free to report bugs and contribute, but remember, there must be **no upload of any copyrighted asset**. 
Run `./extract_assets.py --clean && make clean` or `make distclean` to remove ROM-originated content.

Please contribute **first** to the [nightly branch](https://github.com/sm64pc/sm64ex/tree/nightly/). New functionality will be merged to master once they're considered to be well-tested.

## New features

 * Options menu with various settings, including button remapping.
 * Optional external data loading (so far only textures and assembled soundbanks), providing support for custom texture packs.
 * Optional analog camera and mouse look (using [Puppycam](https://github.com/FazanaJ/puppycam)).
 * Optional OpenGL1.3-based renderer for older machines, as well as the original GL2.1, D3D11 and D3D12 renderers from Emill's [n64-fast3d-engine](https://github.com/Emill/n64-fast3d-engine/).
 * Option to disable drawing distances.
 * Optional model and texture fixes (e.g. the smoke texture).
 * Skip introductory Peach & Lakitu cutscenes with the `--skip-intro` CLI option
 * Cheats menu in Options (activate with `--cheats` or by pressing L thrice in the pause menu).
 * Support for both little-endian and big-endian save files (meaning you can use save files from both sm64-port and most emulators), as well as an optional text-based save format.

Recent changes in Nightly have moved the save and configuration file path to `%HOMEPATH%\AppData\Roaming\sm64ex` on Windows and `$HOME/.local/share/sm64ex` on Linux. This behaviour can be changed with the `--savepath` CLI option.
For example `--savepath .` will read saves from the current directory (which not always matches the exe directory, but most of the time it does);
   `--savepath '!'` will read saves from the executable directory.


